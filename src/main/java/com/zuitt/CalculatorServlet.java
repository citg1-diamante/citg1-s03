package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException{
		System.out.println("**********"); 
		System.out.println("Initialized connection to database"); 
		System.out.println("**********"); 
	}

	public void destroy(){
		System.out.println("**********"); 
		System.out.println("Disconnected"); 
		System.out.println("**********"); 
	}
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		System.out.println("Hellow from the calculator servlet");
		int total = 0; 
		
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2")); 
		String operator = req.getParameter(("operator")); 
		
		PrintWriter out = res.getWriter(); 
		
		if(operator.equals("add")) {
			total = num1+num2; 
			System.out.println(total); 
		}else if (operator.equals("subtract")){
			total = num1-num2; 
			System.out.println(total); 
		}else if (operator.equals("multiply")){
			total = num1*num2; 
			System.out.println(total); 
		}else if(operator.equals("divide")) {
			total = num1/num2; 
			System.out.println(total); 
		}
		
		out.println("<p>The numbers you provided are:" + num1 + ", " + num2 + "<br> The operation that you wanted is: " + operator + "<br>The result is: " + total);
		
		
		
		
			
		
		
		

		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException{
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app</h1><p>To use the app, input two numbers and an operation. <br> Hit the submit button after filling in the details. <br> You will get the result shown in your browser!</p>");
		
	}
	
	
}


